from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import svm

from sklearn.calibration import CalibratedClassifierCV
from sklearn.preprocessing import MinMaxScaler
from imblearn.over_sampling import ADASYN
from sklearn.impute import KNNImputer

from sklearn.inspection import permutation_importance
from sklearn.metrics import brier_score_loss
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.metrics import f1_score

import pandas as pd
import numpy as np
import time
import sys
import os


CROSS_VALDIDATION_FOLDS = 10

np.set_printoptions(threshold=sys.maxsize)

# Naive Bayes
def trainNB(xTrainFold, yTrainFold, xTestFold, yTestFold):
    t0 = time.time()
    naiveBayes = GaussianNB()
    yPred = naiveBayes.fit(xTrainFold, yTrainFold).predict(xTestFold)
    probability = naiveBayes.predict_proba(xTestFold)
    probability = probability[:, 1]
    t1 = time.time()

    execTime = (t1 - t0) * 1000
    accuracy = accuracy_score(yTestFold, yPred)
    F1score = f1_score(yTestFold, yPred, average="binary")
    brierScore = brier_score_loss(yTestFold, probability)
    fpr, tpr, threshold = roc_curve(yTestFold, probability, pos_label=1)
    rocAuc = roc_auc_score(yTestFold, probability)

    confusionMatrix = calcTruePositiveFalsePositive(yTestFold, yPred)
    r = permutation_importance(
        naiveBayes, xTestFold, yTestFold, n_repeats=10, random_state=0
    )

    return (
        accuracy,
        execTime,
        F1score,
        brierScore,
        r.importances_mean,
        rocAuc,
        tpr.flatten(),
        fpr.flatten(),
        confusionMatrix,
    )


# Logistic regression
def trainLogReg(xTrainFold, yTrainFold, xTestFold, yTestFold):
    t0 = time.time()
    logisticRegression = LogisticRegression(solver="liblinear", random_state=0)
    yPred = logisticRegression.fit(xTrainFold, yTrainFold).predict(xTestFold)
    probability = logisticRegression.predict_proba(xTestFold)
    probability = probability[:, 1]
    t1 = time.time()

    execTime = (t1 - t0) * 1000
    accuracy = accuracy_score(yTestFold, yPred)
    F1score = f1_score(yTestFold, yPred, average="binary")
    brierScore = brier_score_loss(yTestFold, probability)
    fpr, tpr, threshold = roc_curve(yTestFold, probability, pos_label=1)
    rocAuc = roc_auc_score(yTestFold, probability)

    confusionMatrix = calcTruePositiveFalsePositive(yTestFold, yPred)
    r = permutation_importance(
        logisticRegression, xTestFold, yTestFold, n_repeats=10, random_state=0
    )

    return (
        accuracy,
        execTime,
        F1score,
        brierScore,
        r.importances_mean,
        rocAuc,
        tpr.flatten(),
        fpr.flatten(),
        confusionMatrix,
    )


# Random forest
def trainRF(xTrainFold, yTrainFold, xTestFold, yTestFold):
    t0 = time.time()
    randomForest = RandomForestClassifier(random_state=0, n_estimators=100)
    yPred = randomForest.fit(xTrainFold, yTrainFold).predict(xTestFold)
    probability = randomForest.predict_proba(xTestFold)
    probability = probability[:, 1]
    t1 = time.time()

    execTime = (t1 - t0) * 1000
    accuracy = accuracy_score(yTestFold, yPred)
    F1score = f1_score(yTestFold, yPred, average="binary")
    brierScore = brier_score_loss(yTestFold, probability)
    fpr, tpr, threshold = roc_curve(yTestFold, probability, pos_label=1)
    rocAuc = roc_auc_score(yTestFold, probability)

    confusionMatrix = calcTruePositiveFalsePositive(yTestFold, yPred)
    r = permutation_importance(
        randomForest, xTestFold, yTestFold, n_repeats=10, random_state=0
    )

    return (
        accuracy,
        execTime,
        F1score,
        brierScore,
        r.importances_mean,
        rocAuc,
        tpr.flatten(),
        fpr.flatten(),
        confusionMatrix,
    )


# Support vector machine
def trainSVM(xTrainFold, yTrainFold, xTestFold, yTestFold):
    scaler = MinMaxScaler()
    scaler.fit(xTrainFold).transform(xTrainFold)
    scaler.fit(xTestFold).transform(xTestFold)

    t0 = time.time()
    supportVectorMachine = svm.SVC(kernel="linear", probability=True, max_iter=10000000)
    supportVectorMachine.fit(xTrainFold, yTrainFold)
    calib_model = CalibratedClassifierCV(supportVectorMachine, method="sigmoid", cv=5)
    calib_model.fit(xTrainFold, yTrainFold)
    yPred = calib_model.predict(xTestFold)
    probability = calib_model.predict_proba(xTestFold)[:, 1]
    t1 = time.time()

    execTime = (t1 - t0) * 1000
    accuracy = accuracy_score(yTestFold, yPred)
    F1score = f1_score(yTestFold, yPred, average="binary")
    brierScore = brier_score_loss(yTestFold, probability)
    fpr, tpr, threshold = roc_curve(yTestFold, probability, pos_label=1)
    rocAuc = roc_auc_score(yTestFold, probability)

    confusionMatrix = calcTruePositiveFalsePositive(yTestFold, yPred)
    r = permutation_importance(
        supportVectorMachine, xTestFold, yTestFold, n_repeats=10, random_state=0
    )

    return (
        accuracy,
        execTime,
        F1score,
        brierScore,
        r.importances_mean,
        rocAuc,
        tpr.flatten(),
        fpr.flatten(),
        confusionMatrix,
    )


# Gradient boosting
def trainGB(xTrainFold, yTrainFold, xTestFold, yTestFold):
    t0 = time.time()
    gradientBoosting = GradientBoostingClassifier(
        n_estimators=100, learning_rate=0.1, random_state=0
    )
    yPred = gradientBoosting.fit(xTrainFold, yTrainFold).predict(xTestFold)
    probability = gradientBoosting.predict_proba(xTestFold)
    probability = probability[:, 1]
    t1 = time.time()

    execTime = (t1 - t0) * 1000
    accuracy = accuracy_score(yTestFold, yPred)
    F1score = f1_score(yTestFold, yPred, average="binary")
    brierScore = brier_score_loss(yTestFold, probability)
    fpr, tpr, threshold = roc_curve(yTestFold, probability, pos_label=1)
    rocAuc = roc_auc_score(yTestFold, probability)

    confusionMatrix = calcTruePositiveFalsePositive(yTestFold, yPred)
    r = permutation_importance(
        gradientBoosting, xTestFold, yTestFold, n_repeats=10, random_state=0
    )

    return (
        accuracy,
        execTime,
        F1score,
        brierScore,
        r.importances_mean,
        rocAuc,
        tpr.flatten(),
        fpr.flatten(),
        confusionMatrix,
    )


def calcTruePositiveFalsePositive(y, yPred):
    TN, FP, FN, TP = confusion_matrix(y, yPred).ravel()
    confusionMatrix = [TN, FP, FN, TP]

    return confusionMatrix


def runModel(X, y, trainFunction, algorithmName, featureNames):
    accuracy = []
    execTime = []
    f1Score = []
    brierScore = []
    rocAUC = []
    truePositiveRate = []
    falsePositiveRate = []
    featureImportance = []
    confusionMatrix = []

    skf = StratifiedKFold(n_splits=10)
    ada = ADASYN(random_state=42)
    validationRound = 1
    print("Rounds finished: ")

    # 10 fold stratified cross-validation
    for trainIndex, testIndex in skf.split(X, y):
        xTrainFold, xTestFold = X[trainIndex], X[testIndex]
        yTrainFold, yTestFold = y[trainIndex], y[testIndex]

        xTrainFold, yTrainFold = ada.fit_resample(xTrainFold, yTrainFold)
        xTestFold, yTestFold = ada.fit_resample(xTestFold, yTestFold)
        (
            tempAcc,
            tempTime,
            tempFscore,
            tempBrier,
            tempImportance,
            tempRoc,
            tempTpr,
            tempFpr,
            tempConfusionMatrix,
        ) = trainFunction(xTrainFold, yTrainFold, xTestFold, yTestFold)

        accuracy.append(tempAcc)
        execTime.append(tempTime)
        f1Score.append(tempFscore)
        brierScore.append(tempBrier)
        rocAUC.append(tempRoc)
        truePositiveRate.append(tempTpr)
        falsePositiveRate.append(tempFpr)
        featureImportance.append(tempImportance)
        confusionMatrix.append(tempConfusionMatrix)

        print(str(validationRound) + ", ", end="", flush=True)
        validationRound += 1

    print("\n\n\x1b[6;30;42m" + "Training completed" + "\x1b[0m")
    writeResultsToFile(
        accuracy,
        execTime,
        f1Score,
        brierScore,
        featureImportance,
        rocAUC,
        truePositiveRate,
        falsePositiveRate,
        confusionMatrix,
        algorithmName,
        featureNames,
    )
    print("\x1b[6;30;42m" + "Write to file completed" + "\x1b[0m")


def writeResultsToFile(
    accuracy,
    execTime,
    f1Score,
    brierScore,
    featureImportance,
    rocAUC,
    truePositiveRate,
    falsePositiveRate,
    confusionMatrix,
    algorithmName,
    featureNames,
):
    filepath = "./results/" + algorithmName + ".txt"
    if os.path.exists(filepath):
        os.remove(filepath)
    f = open(filepath, "a")

    nrFeatures = len(featureImportance[0])

    for i in range(nrFeatures):
        f.write(str(featureNames[i]) + "\t")

    f.write("\n")
    for i in range(len(featureImportance)):
        for j in range(nrFeatures):
            f.write(str(featureImportance[i][j]) + "\t")
        f.write("\n")

    f.write("\n")
    f.write("accuracy: \n")
    for singleAccuracy in accuracy:
        f.write(str(singleAccuracy) + "\n")
    f.write("\n")
    f.write("avg: " + str(np.mean(accuracy)) + "\n")
    f.write("std dev: " + str(np.std(accuracy)) + "\n")
    f.write("\n")

    f.write("time: \n")
    for singleExecTime in execTime:
        f.write(str(singleExecTime) + "\n")
    f.write("\n")
    f.write("avg: " + str(np.mean(execTime)) + "\n")
    f.write("std dev: " + str(np.std(execTime)) + "\n")
    f.write("\n")

    f.write("f1 score: \n")
    for singleF1Score in f1Score:
        f.write(str(singleF1Score) + "\n")
    f.write("\n")
    f.write("avg: " + str(np.mean(f1Score)) + "\n")
    f.write("std dev: " + str(np.std(f1Score)) + "\n")
    f.write("\n")

    f.write("brier score: \n")
    for singleBrierScore in brierScore:
        f.write(str(singleBrierScore) + "\n")
    f.write("\n")
    f.write("avg: " + str(np.mean(brierScore)) + "\n")
    f.write("std dev: " + str(np.std(brierScore)) + "\n")
    f.write("\n")

    f.write("ROC AUC: \n")
    for singleAUC in rocAUC:
        f.write(str(singleAUC) + "\n")
    f.write("\n")
    f.write("avg: " + str(np.mean(rocAUC)) + "\n")
    f.write("std dev: " + str(np.std(rocAUC)) + "\n")
    f.write("\n")

    f.write("\n")
    f.write("TN, FP, FN, TP: \n")
    f.write("\n")
    for i in range(CROSS_VALDIDATION_FOLDS):
        for j in range(len(confusionMatrix[i])):
            f.write(str(confusionMatrix[i][j]) + "\t")
        f.write("\n")

    f.write("True Positive Rate:: \n")
    for i in range(CROSS_VALDIDATION_FOLDS):
        f.write(str(i) + "\n")
        for j in range(len(truePositiveRate[i])):
            f.write(str(truePositiveRate[i][j]) + "\n")
        f.write("\n")

    f.write("\n")
    f.write("False Positive Rate: \n")
    for i in range(CROSS_VALDIDATION_FOLDS):
        f.write(str(i) + "\n")
        for j in range(len(falsePositiveRate[i])):
            f.write(str(falsePositiveRate[i][j]) + "\n")
        f.write("\n")

    f.close()


def readData():
    data = pd.read_csv("./data/data.csv", usecols=range(16), sep=",")
    data = np.array(data)

    hasDementia = pd.read_csv("./data/data.csv", usecols=[16], sep=",")
    hasDementia.to_numpy()
    hasDementia = np.array(hasDementia)
    hasDementia = hasDementia.ravel()

    imputer = KNNImputer(n_neighbors=5, weights="uniform")
    data = imputer.fit_transform(data)

    return data, hasDementia


def main():
    data, hasDementia = readData()
    featureNames = [
        "Gender",
        "AGE",
        "A1",
        "C51_Education",
        "D31",
        "D32",
        "D33",
        "D34",
        "D35",
        "D36",
        "D37",
        "D38",
        "D39",
        "D40",
        "E36",
        "E135",
    ]
    print("-------------\tSVM\t-------------------")
    runModel(data, hasDementia, trainSVM, "SVM", featureNames)
    print()

    print("-------------\tGradient boosting\t-------------------")
    runModel(data, hasDementia, trainGB, "GB", featureNames)
    print()

    print("-------------\tNaive Bayes\t-------------------")
    runModel(data, hasDementia, trainNB, "NB", featureNames)
    print()

    print("-------------\tLogistic regression\t-------------------")
    runModel(data, hasDementia, trainLogReg, "LR", featureNames)
    print()

    print("-------------\tRandom Forest\t-------------------")
    runModel(data, hasDementia, trainRF, "RF", featureNames)
    print()


if __name__ == "__main__":
    main()
